﻿#include <iostream>

//Вызывает функцию std::cout
void print()
{
    /*Потому-что хотим использовать стандартную библиотеку
      для печати в консоль и неиспользовать какие-либо другие. */
    std::cout << "Hello Pavel!\n";
}

//Присваиваем значение переменным.
int main()
{
    int x = 100;
    int y = x + 100;

    int b = 0;
    b = b + 2;

    int test;
    test = 100500;

    int test2 = 1005001;


    int mult = x * y;
    int random = 1002;

    //Вызвал другую переменную, нежели в уроке.
    std::cout << mult;
}
